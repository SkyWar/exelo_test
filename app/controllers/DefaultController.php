<?php

namespace app\controllers;

use app\models\Proxy as arProxy;
use Yii;

class DefaultController extends \yii\console\Controller
{
    function actionRandom($num = 10)
    {
        if (!is_numeric($num)) {
            throw new \InvalidArgumentException("Wrong param type.");
        }

        $query = arProxy::find()
            ->orderBy('RAND()')
            ->limit($num);

        /** @var arProxy $iProxy */
        foreach ($query->each() as $iProxy) {
            $ip = long2ip($iProxy->ip);
            $port = $iProxy->port;

            $this->stdout("{$ip}:{$port}\n");
        }
    }

    function actionGenerate($url = 'https://gist.githubusercontent.com/mrsombre/12c2b14225e096ba898d0e2e97e25510/raw/00783b601a6a67e662781e7351c8ddd1a0ade9cb/test.htm'){
        //получаем новые данные из HTMLDOM
        $data = arProxy::serialize($url);
        //получаем старые данные из MYSQL
        $old_ip = arProxy::get_all();
        $search_array = [];

        if ($data){
            //запись в лог если данные изменились
            Yii::error($old_ip, $category = 'proxy');

            //обновление
            foreach ($data as $address){
                $find = arProxy::get_ip($address['ip']);
                if ($find){
                    if ($find['port'] != $address['port']){
                        arProxy::update_port($address['ip'], $address['port']);
                    }
                }
                else{
                    arProxy::save_ip($address['ip'], $address['port']);
                }
                //записываем все новые ip-адреса
                $search_array[] = $address['ip'];
            }

            //удаление
            foreach ($old_ip as $old_address){
                $find = array_search(long2ip($old_address['ip']), $search_array);

                if ($find === false) {
                    echo "Неактуальный ip : ".$old_address['ip']."\n\r";
                    arProxy::del_ip($old_address['ip']);
                }
            }
        } else {
            echo "Данные актуальны \n\r";
        }
    }

}
    
