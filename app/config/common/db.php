<?php

use yii\helpers\ArrayHelper;

$config = [
    'host' => 'localhost',
    'dbname' => 'test1',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8'
];

// локальные настройки
if (file_exists(__DIR__ . '/db.local.php')) {
    $config = ArrayHelper::merge($config, require(__DIR__ . '/db.local.php'));
}

return $config;
