<?php

use yii\helpers\ArrayHelper;

// database config
$db = require(__DIR__ . '/db.php');

$dateYmd = date('Ymd');

$config = [
    'basePath' => APP_PATH_ROOT . '/app',
    'runtimePath' => APP_PATH_ROOT . '/runtime',
    'vendorPath' => APP_PATH_ROOT . '/vendor',
    'aliases' => [
        '@root' => APP_PATH_ROOT
    ],
    'components' => [
        'db' => [
            'class' => yii\db\Connection::class,
            'dsn' => "mysql:host={$db['host']};dbname={$db['dbname']}",
            'username' => $db['username'],
            'password' => $db['password'],
            'charset' => $db['charset'],
            // caching
            'enableSchemaCache' => true,
            'schemaCache' => 'cache'
        ],
        // cache
        'cache' => [
            'class' => yii\caching\FileCache::class
        ]
    ],
    'timeZone' => 'Europe/Moscow'
];

// локальные настройки
if (file_exists(__DIR__ . '/yii.local.php')) {
    $config = ArrayHelper::merge($config, require(__DIR__ . '/yii.local.php'));
}

return $config;
