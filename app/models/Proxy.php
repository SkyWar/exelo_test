<?php

namespace app\models;

use Yii;

class Proxy extends \yii\db\ActiveRecord
{
    static function tableName()
    {
        return 'proxy';
    }

    function beforeSave($insert)
    {
        if ($insert) {
            $this->timeCreated = time();
        }

        return parent::beforeSave($insert);
    }

    public static function serialize($url){

        try {
            $html = file_get_html($url);
        } catch (Exception $e) {
            Yii::error($e, $category = 'proxy');
            echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
            die();
        }

        $last_modify = Yii::$app->cache;

        //вычисляем md5 для проверки изменения данных
        if (md5($html) === $last_modify->get('last')){
            return false;
        }
        else{
            $last_modify->set('last', md5($html));
        }

        $rowData = array();
        $ip_port = array();

        //формируем массивы из записей в tr
        foreach($html->find('tr') as $row) {
            $data = array();
            foreach($row->find('td') as $cell) {
                // записываем в массив и убираем лишние символы
                $data[] = str_replace(array(' ', '/', ','), '', $cell->plaintext);
            }
            $rowData[] = $data;
        }

        //получаем ip, port, а так же производим валидацию ip
        foreach ($rowData as $value){
            if (isset($value[0]) and filter_var($value[0], FILTER_VALIDATE_IP)){
                $ip_port[] = ['ip'=>$value[0], 'port'=>$value[1]];
            }
        }

        return $ip_port;
    }

    public static function del_ip($ip){
        Yii::$app->db->createCommand("DELETE FROM proxy WHERE ip=:ip")
            ->bindValue(':ip', $ip)
            ->execute();
    }

    public static function get_ip($ip){
        $data = Yii::$app->db->createCommand("SELECT * FROM proxy WHERE ip=INET_ATON(:ip)")
            ->bindValue(':ip', $ip)
            ->queryOne();
        return $data;
    }

    public static function get_all(){
        $data = Yii::$app->db->createCommand("SELECT ip, port FROM proxy")
            ->queryAll();
        return $data;
    }

    public static function update_port($ip, $port){
        Yii::$app->db->createCommand("UPDATE proxy SET port = :port, timeCreated=:timeCreated where ip=INET_ATON(:ip)")
            ->bindValue(':ip', $ip)
            ->bindValue(':port', $port)
            ->bindValue(':timeCreated', date_timestamp_get(date_create()))
            ->execute();
    }

    public static function save_ip($ip, $port){
        $data = Yii::$app->db->createCommand("INSERT INTO proxy VALUES (DEFAULT , INET_ATON(:ip), :port, :timeCreated)")
            ->bindValue(':ip', $ip)
            ->bindValue(':port', $port)
            ->bindValue(':timeCreated', date_timestamp_get(date_create()))
            ->execute();

        return $data;
    }

}
