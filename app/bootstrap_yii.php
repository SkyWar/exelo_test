<?php

if (!defined('YII_DEBUG')) {
    define('YII_DEBUG', APP_DEBUG);
}
if (!defined('YII_ENV')) {
    define('YII_ENV', APP_ENVIRONMENT);
}
if (!defined('YII_ENV_PROD')) {
    define('YII_ENV_PROD', APP_ENVIRONMENT === APP_ENV_PRODUCTION);
}
if (!defined('YII_ENV_DEV')) {
    define('YII_ENV_DEV', APP_ENVIRONMENT === APP_ENV_DEVELOPMENT);
}
if (!defined('YII_ENV_TEST')) {
    define('YII_ENV_TEST', APP_ENVIRONMENT === APP_ENV_TEST);
}
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
